const config = require('./config/config'),
      Mongo = require('./config/dbConnection'),
      db = new Mongo(config.mongo),
      app = require('./config/server'),
      http = require('http'),
      asyncHook = require('./config/asyncHook')


asyncHook.enable()

/**
 * Create HTTP server.
 */
const server = http.createServer(app)
server.listen(config.server.port)

process.on('SIGINT', () => {
    asyncHook.disable()
    db.close()
    server.close()
})

process.on('SIGTERM', () => {
    asyncHook.disable()
    db.close()
    server.close()
})

process.on('unhandledRejection', (err, p) => {
    console.error('Unhandled Rejection:', err.message)
})

module.exports = server