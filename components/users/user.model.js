const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Enter name'],
        validate: {
            validator: v => /[A-Za-z]*/.test(v),
            message: 'Name should only contain characters'
        }
    },
    age: {
        type: Number,
        required: true,
        min: [18, 'Need to be over 18 years']
    },
    genre: {
        type: String,
        enum: ['Male', 'Female']
    },
    city: String,
    street: String
})

schema.pre('save', next => {
    console.log(`The user has saved`)
    next()
})

module.exports = mongoose.model('User', schema)