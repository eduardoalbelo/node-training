/**
 * User Controller
 */

const express = require('express'),
      router = express.Router(),
      { User } = require('./index')

router.get('/', async (req, res, next) => {
    try {
        const users = await User.load()
        res.json(users)
    } catch (err) {
        next(err)
    }
})

router.post('/', async (req, res, next) => {
    try {
        const user = new User(req.body)
        const newUser = await user.save()
        res.json(newUser)
    } catch (err) {
        next(err)
    }
})

router.get('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const user = await User.load(req.params.id)
        res.json(user)
    } catch (err) {
        next(err)
    }
});

router.put('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const opt = {
            new: true,
            runValidators: true
        }
        const user = await User.findOneAndUpdate({_id: req.params.id}, req.body, opt)
        res.json(user)
    } catch (err) {
        next(err)
    }
});

router.delete('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const result = await User.findOneAndDelete({_id: req.params.id})
        res.json(result)
    } catch (err) {
        next(err)
    }
});

module.exports = router