const UserDb = require('./user.model')

module.exports = class User extends UserDb {

    constructor(args) {
        super(args)
    }

    static async load(id = null) {
        return await super.find(id ? {_id: id} : {}, null, {
            limit: 99,
            sort: {
                age: 1
            }
        })
    }
}