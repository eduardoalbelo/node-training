const User = require('./user')

module.exports = {
  User: User,
  Auth: passport.authenticate('bearer', { session: false })
}