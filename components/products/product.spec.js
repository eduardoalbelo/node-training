const assert = require('assert'),
      { Product } = require('./index')

describe('Product module', () => {

    it('Cleanup documents', async () => {
        const err = await Product.deleteMany({})
        const results = await Product.load()
        assert.strictEqual(results.length, 0)
    })
    
})