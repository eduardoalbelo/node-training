const ProductDb = require('./product.model')

module.exports = class Product extends ProductDb {

    constructor(args) {
        super(args)
    }

    static async load(id = null) {
        return await super.find(id ? {_id: id} : {}, null, {
            limit: 99,
            sort: {
                title: 1
            }
        })
    }
}