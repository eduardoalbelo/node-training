/**
 * Product Controller
 */

const express = require('express'),
      router = express.Router(),
      { Product } = require('./index')

router.get('/', async (req, res, next) => {
    try {
        const products = await Product.load()
        res.json(products)
    } catch (err) {
        next(err)
    }
})

router.post('/', async (req, res, next) => {
    try {
        const product = new Product(req.body)
        const newProduct = await product.save()
        
        res.json(newProduct)
    } catch (err) {
        next(err)
    }
})

router.get('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const product = await Product.load(req.params.id)
        res.json(product)
    } catch (err) {
        next(err)
    }
});

router.put('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const opt = {
            new: true,
            runValidators: true
        }
        const product = await Product.findOneAndUpdate({_id: req.params.id}, req.body, opt)
        res.json(product)
    } catch (err) {
        next(err)
    }
});

router.delete('/:id([a-z0-9]+)', async (req, res, next) => {
    try {
        const product = await Product.findOneAndDelete({_id: req.params.id})
        res.json(product)
    } catch (err) {
        next(err)
    }
});

module.exports = router