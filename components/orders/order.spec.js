const assert = require('assert'),
      { Order } = require('./index')

describe('Order module', () => {

    it('Cleanup documents', async () => {
        const err = await Order.deleteMany({})
        const results = await Order.load()
        assert.strictEqual(results.length, 0)
    })
    
})