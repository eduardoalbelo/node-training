/**
 * Order Controller
 */

const express = require('express'),
      router = express.Router(),
      { Order } = require('./index'),
      { Auth } = require('../users')

router.get('/', Auth, async (req, res, next) => {
    try {
        const orders = await Order.load()
        res.json(orders)
    } catch (err) {
        next(err)
    }
})

router.post('/', Auth, async (req, res, next) => {
    try {
        const order = new Order(req.body)
        const newOrder = await order.save()
        res.json(newOrder)
    } catch (err) {
        next(err)
    }
})

router.get('/:id([a-z0-9]+)', Auth, async (req, res, next) => {
    try {
        const order = await Order.load(req.params.id)
        res.json(order)
    } catch (err) {
        next(err)
    }
});

router.put('/:id([a-z0-9]+)', Auth, async (req, res, next) => {
    try {
        const opt = {
            new: true,
            runValidators: true
        }
        const order = await Order.findOneAndUpdate({_id: req.params.id}, req.body, opt)
        res.json(order)
    } catch (err) {
        next(err)
    }
});

router.delete('/:id([a-z0-9]+)', Auth, async (req, res, next) => {
    try {
        const order = await Order.findOneAndDelete({_id: req.params.id})
        res.json(order)
    } catch (err) {
        next(err)
    }
});

module.exports = router