const OrderDb = require('./order.model')

module.exports = class Order extends OrderDb {

    constructor(args) {
        super(args)
    }

    static async load(id = null) {
        return await super.find(id ? {_id: id} : {}, null, {
            limit: 99,
            sort: {
                title: 1
            }
        })
    }
}