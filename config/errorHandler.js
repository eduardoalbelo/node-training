module.exports = (err, req, res, next) => {

    if (res.headersSent)
        next(err)
    
    console.error(err.stack)
    res.status(err.code || 500)
    res.json({error: err.message})
}