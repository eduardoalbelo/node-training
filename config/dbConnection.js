const mongoose = require('mongoose')

module.exports = class MongoDriver {

    constructor(config) {
        this.host = config.host
        this.port = config.port
        this.db = config.db
        this.open()
    }

    open() {
        
        mongoose.connect(`mongodb://${this.host}:${this.port}/${this.db}`, {
            useNewUrlParser: true
        })

        mongoose.set('useFindAndModify', false)

        this.listen('connected', err => {
            console.log(`Mongoose connected`)
        })

        this.listen('error', err => {
            console.log(`Mongoose connection has occured ${err} error`)
        })
    }

    listen(event, callback) {
        mongoose.connection.on(event, callback)
    }

    close() {
        mongoose.connection.close(() => {
            console.log('Mongoose disconnected')
            process.exit(0)
        })
    }
}