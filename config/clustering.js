const cluster = require('cluster'),
      numCPUs = require('os').cpus().length

const masterProcess = () => {

    console.log(`Master ${process.pid} is running`)

    for (let i = 0; i < numCPUs; i++) {
        console.log(`Forking process number ${i}...`)
        const worker = cluster.fork()
    }

    process.exit()

}

const childProcess = () => {
    console.log(`Worker ${process.pid} started and finished`)
    process.exit()
}

if (cluster.isMaster)
  masterProcess()
else
  childProcess()