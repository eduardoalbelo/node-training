const async_hooks = require('async_hooks'),
      fs = require('fs')

const init = (asyncId, type, triggerAsyncId, resource) => {
    //fs.writeSync(1, `${type}(${asyncId}) | trigger: ${triggerAsyncId}\n`)
}

const before = (asyncId) => { }

const after = (asyncId) => { }

const destroy = (asyncId) => { }

const promiseResolve = (asyncId) => { }

module.exports = async_hooks.createHook({ init, before, after, destroy, promiseResolve })