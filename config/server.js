const bodyParser = require('body-parser'),
      methodOverride = require('method-override'),
      express = require('express'),
      app = express(),
      logger = require('./logger'),
      errorHandler = require('./errorHandler'),
      cors = require('cors')
      passport = require('passport')
      require('./passport')

app.use(cors())

app.use(bodyParser.urlencoded({
    'extended': 'true'
}))

app.use(bodyParser.json())

app.use(methodOverride('X-HTTP-Method-Override'))

app.use(logger)

app.use(express.static('public'))

;(async () => {
    await require('./loadRoutes')(app)
    app.use((req, res, next) => res.status(404).send('Sorry can\'t find that!'))
    app.use(errorHandler)
})()

module.exports = app