const morgan = require('morgan'),
      fs = require('fs'),
      path = require('path'),
      config = require('./config')

const accessLogStream = fs.createWriteStream(path.join(__dirname, '..', 'log', 'error.log'), { flags: 'a' })

module.exports = morgan(config.logger.level, {
    skip: (req, res) => res.statusCode < 400 && config.logger.enabled,
    stream: accessLogStream
})